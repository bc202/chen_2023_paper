all_genes = read.csv("~/cac/final/dataframe/gene_master.csv")
yeast_gene = subset(all_genes,chr!='chrM' & all_genes$overlap==F)
f_col=brewer.pal(6,"Blues")
n = nrow(yeast_gene)

read.count = read_csv('~/cac/final/bam_bai/RNA/rep1/read_count.csv')
#get WT TPM
# wt_rna_sense_M_tpm = data.frame("gene" =yeast_gene$gene, "af"=rep(0,n),"s10"=rep(0,n),"s20"=rep(0,n),"s30"=rep(0,n),"s40"=rep(0,n),"s50"=rep(0,n),"s60"=rep(0,n),"s150"=rep(0,n))
# wt_rna_AS_M_tpm = data.frame("gene" =yeast_gene$gene, "af"=rep(0,n),"s10"=rep(0,n),"s20"=rep(0,n),"s30"=rep(0,n),"s40"=rep(0,n),"s50"=rep(0,n),"s60"=rep(0,n),"s150"=rep(0,n))
# F_list = c("wt_af_F","wt_10_F","wt_20_F","wt_30_F","wt_40_F","wt_50_F","wt_60_F","wt_150_F")
# R_list =c("wt_af_R","wt_10_R","wt_20_R","wt_30_R","wt_40_R","wt_50_R","wt_60_R","wt_150_R")

#get cac TPM
cac_rna_sense_M_tpm = data.frame("gene" =yeast_gene$gene, "af"=rep(0,n),"s10"=rep(0,n),"s20"=rep(0,n),"s30"=rep(0,n),"s40"=rep(0,n),"s50"=rep(0,n),"s60"=rep(0,n),"s150"=rep(0,n))
cac_rna_AS_M_tpm = data.frame("gene" =yeast_gene$gene, "af"=rep(0,n),"s10"=rep(0,n),"s20"=rep(0,n),"s30"=rep(0,n),"s40"=rep(0,n),"s50"=rep(0,n),"s60"=rep(0,n),"s150"=rep(0,n))
F_list = c("cac_af_F","cac_10_F","cac_20_F","cac_30_F","cac_40_F","cac_50_F","cac_60_F","cac_150_F")
R_list = c("cac_af_R","cac_10_R","cac_20_R","cac_30_R","cac_40_R","cac_50_R","cac_60_R","cac_150_R")



for (k in 1:8) {
  for(i in 1:nrow(yeast_gene)){
    if(i %% 500 ==0){
      cat('Gene #',i,'\n',sep='')
    }
    chr=yeast_gene$chr[i]
    new_start = yeast_gene$start[i]
    new_end = yeast_gene$end[i]
    
    if(yeast_gene$strand[i] =="+"){
      chr.gr = GRanges(seqnames= chr, ranges = IRanges(start =new_start , end = new_end ))
      
      p = ScanBamParam(what = c("rname", "strand", "pos", "isize"),which = chr.gr)
      
      file_name.bam_A = paste("~/cac/final/bam_bai/RNA/rep1/",F_list[k],".bam", sep='')
      file_name.bam.bai_A = paste("~/cac/final/bam_bai/RNA/rep1/",F_list[k],".bam.bai", sep='') 
      
      file_name.bam_B = paste("~/cac/final/bam_bai/RNA/rep1/",R_list[k],".bam", sep='')
      file_name.bam.bai_B = paste("~/cac/final/bam_bai/RNA/rep1/",R_list[k],".bam.bai", sep='')
      
      A_reads.l = scanBam(file = file_name.bam_A,
                          index = file_name.bam.bai_A,
                          param = p)
      
      B_reads.l = scanBam(file = file_name.bam_B,
                          index = file_name.bam.bai_B,
                          param = p)
      
      
      #create a new GenomicRanges object for the reads from this list:
      A_reads.gr = GRanges(seqnames = A_reads.l[[1]]$rname,
                           ranges = IRanges(start = A_reads.l[[1]]$pos,
                                            width = A_reads.l[[1]]$isize))
      
      B_reads.gr = GRanges(seqnames = B_reads.l[[1]]$rname,
                           ranges = IRanges(start = B_reads.l[[1]]$pos,
                                            width = B_reads.l[[1]]$isize))
      
      cac_rna_sense_M_tpm[i,k+1] = (length(A_reads.gr) / (new_end - new_start)) / read.count$F_count[k+8] * 1000000*50
      cac_rna_AS_M_tpm[i,k+1] = (length(B_reads.gr) / (new_end - new_start)) / read.count$R_count[k+8] * 1000000*50
      
    }else{
      chr.gr = GRanges(seqnames= chr, ranges = IRanges(start =new_start , end = new_end ))
      
      p = ScanBamParam(what = c("rname", "strand", "pos", "isize"),which = chr.gr)
      
      file_name.bam_A = paste("~/cac/final/bam_bai/RNA/rep1/",R_list[k],".bam", sep='')
      file_name.bam.bai_A = paste("~/cac/final/bam_bai/RNA/rep1/",R_list[k],".bam.bai", sep='') 
      
      file_name.bam_B = paste("~/cac/final/bam_bai/RNA/rep1/",F_list[k],".bam", sep='')
      file_name.bam.bai_B = paste("~/cac/final/bam_bai/RNA/rep1/",F_list[k],".bam.bai", sep='')
      
      A_reads.l = scanBam(file = file_name.bam_A,
                          index = file_name.bam.bai_A,
                          param = p)
      
      B_reads.l = scanBam(file = file_name.bam_B,
                          index = file_name.bam.bai_B,
                          param = p)
      
      
      #create a new GenomicRanges object for the reads from this list:
      A_reads.gr = GRanges(seqnames = A_reads.l[[1]]$rname,
                           ranges = IRanges(start = A_reads.l[[1]]$pos,
                                            width = A_reads.l[[1]]$isize))
      
      B_reads.gr = GRanges(seqnames = B_reads.l[[1]]$rname,
                           ranges = IRanges(start = B_reads.l[[1]]$pos,
                                            width = B_reads.l[[1]]$isize))
      
      cac_rna_sense_M_tpm[i,k+1] = (length(A_reads.gr) / (new_end - new_start)) / read.count$R_count[k+8] * 1000000*50
      cac_rna_AS_M_tpm[i,k+1] = (length(B_reads.gr) / (new_end - new_start)) / read.count$F_count[k+8] * 1000000*50
    }
  }
  cat(paste("file # ",k,"\n",sep=""))
}


write.csv(cac_rna_AS_M_tpm,file='~/cac/final/r_file/cac_AS_rep1.csv',row.names = FALSE)
write.csv(cac_rna_sense_M_tpm,file='~/cac/final/r_file/cac_sense_rep1.csv',row.names = FALSE)

#repeat for loop for WT

