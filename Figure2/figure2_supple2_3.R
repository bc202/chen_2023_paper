#nucleosome heatmap processing
nuc_master = read.csv('~/cac/final/r_file/nuc_master.csv')
nuc_master_sub = subset(nuc_master,cac_c40 > quantile(nuc_master$cac_c40,0.25) & wt_c40 > quantile(nuc_master$wt_c40,0.25) ) #high-confidence nuc

###
#figure 2A, B  occu heatmap by ordered by decreasing ATI
###
nuc_chrIV = subset(nuc_master_sub,chr=='chrIV')
nuc_chrIV = nuc_chrIV[order(nuc_chrIV$cac_ATI,decreasing = TRUE),]
cac_nuc_norm = nuc_chrIV[,8:12]/nuc_chrIV[,12] #normalized to the final occupancy 
cac_nuc_norm[cac_nuc_norm >=1.5]=1.499

nuc_chrIV = nuc_chrIV[order(nuc_chrIV$wt_ATI,decreasing =TRUE),]
wt_nuc_norm = nuc_chrIV[,3:7]/nuc_chrIV[,7]
wt_nuc_norm[wt_nuc_norm >=1.5]=1.499

#plotting
scr.m = matrix(c(0.04, 0.98, 0,0.24, # ATI denstity plot
                 
                 0.04, 0.51, 0.3, 0.95, #WT heatmap
                 0.51, 0.98, 0.3, 0.95, # cac heatmap
                 
                 0.5 ,0.95, 0.25,0.3 #heatmap legend
                 
                 
),
ncol = 4, byrow = T
)


file.name <- "/data/home/bc202/cac/final/figure/figure2/figure2A_B_ATI_density_heatmap.png"
png(file = file.name, width =6.5, height = 15, units = "in", res = 280, bg = "white", type = "cairo-png" )
par(cex=1.5)

close.screen(all.screens = T)

split.screen(scr.m)

par(oma = c(0, 1, 0, 1)) # make room (i.e. the 4's) for the overall x and y axis titles

screen(1)
par(mar = c(4,4,1,1), cex=1.6)
cl <- viridis(100, alpha = 1, begin = 0, end = 1, option = "D")
plot(density(nuc_master_sub$wt_ATI,bw=0.005),xlab='ATI',main='',col=cl[55],lwd=3.5,xlim=c(1.2,1.9))
lines(density(nuc_master_sub$cac_ATI,na.rm = TRUE,bw=0.005),col='orange',lwd=3.5)
legend('topleft',c('WT',expression(paste(italic(cac1),Delta))),col=c(cl[55],'orange'),lty=c(1,1),lwd=c(3,3),box.lwd = 0,box.col = "transparent",bg = "transparent")

a=wt_nuc_norm
screen(2)
par(mar = c(1.5,0.5,1.2,0.5), cex=2.5, cex.main=0.8)  #margins order :bottom, left, top, right #cex.main is title magnif. relative to cex
dens_dot_plot(a[nrow(a):1,],  z_min = -0.5, z_max =1.5, plot_title='',x_axt='n',y_axt = "n",lowCol = 'red', medCol = "white", highCol = "blue", numColors = 100) #adjust z range for each experiment
axis(side=1,labels=F, tick=F,at=c(-2,-1,0,1,2)) #x-axis
mtext(at=c(-2,-1,0,1,2), text=c('P','C10','C15','C20','C40'), side=1, line=0, cex=1.5)

a=cac_nuc_norm
screen(3)
par(mar = c(1.5,0.5,1.2,0.5), cex=2.5, cex.main=0.8)  #margins order :bottom, left, top, right #cex.main is title magnif. relative to cex
dens_dot_plot(a[nrow(a):1,],  z_min = -0.5, z_max =1.5, plot_title='',x_axt='n',y_axt = "n",lowCol = 'red', medCol = "white", highCol = "blue", numColors = 100) #adjust z range for each experiment
axis(side=1,labels=F, tick=F,at=c(-2,-1,0,1,2)) #x-axis
mtext(at=c(-2,-1,0,1,2), text=c('P','C10','C15','C20','C40'), side=1, line=0, cex=1.5)

screen(4)
eff_color2 <- matrix(c(0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1,1.1,1.2,1.3,1.4,1.5), nrow=1, ncol=15)
par(mar = c(1,1,1,1))
dens_dot_plot(eff_color2, z_min = -0.5, z_max = 1.5, plot_title="",x_axt='n',y_axt = "n",medCol='white',lowCol = "red", highCol = 'blue',numColors = 100)
mtext(at=c(-7,-3,2,7), text=c(0.1,0.5,1,1.5), side=1,cex=1.2)

close.screen(all.screens = T)

dev.off()



###
#figure 2C, 2D making stack up plot (normalized each decile to the same final occupancy)
####
cac_fast_mtx_list = readRDS('~/cac/final/r_file/cac_fast_mtx_list.RDS')
cac_slow_mtx_list = readRDS('~/cac/final/r_file/cac_slow_mtx_list.RDS')
wt_fast_mtx_list = readRDS('~/cac/final/r_file/wt_fast_mtx_list.RDS')
wt_slow_mtx_list = readRDS('~/cac/final/r_file/wt_slow_mtx_list.RDS')

wt_fast_chase40_sum  = c()
wt_slow_chase40_sum = c()
cac_fast_chase40_sum = c()
cac_slow_chase40_sum = c()

for (i in 1:10) {
  wt_fast_chase40_sum = c(wt_fast_chase40_sum,sum(wt_fast_mtx_list[[i]][[5]]))
  wt_slow_chase40_sum = c(wt_slow_chase40_sum,sum(wt_slow_mtx_list[[i]][[5]]))
  
  cac_fast_chase40_sum = c(cac_fast_chase40_sum,sum(cac_fast_mtx_list[[i]][[5]]))
  cac_slow_chase40_sum = c(cac_slow_chase40_sum,sum(cac_slow_mtx_list[[i]][[5]]))
}

cac_fast_mtx_list_norm = list()
cac_slow_mtx_list_norm = list()
wt_fast_mtx_list_norm = list()
wt_slow_mtx_list_norm = list()

for (i in 1:5) {
  cac_fast_mtx_list_nrom[[i]] = cac_fast_mtx_list[[1]][[i]]
  cac_slow_mtx_list_nrom[[i]] = cac_slow_mtx_list[[1]][[i]]
  wt_fast_mtx_list_norm[[i]] = wt_fast_mtx_list[[1]][[i]]
  wt_slow_mtx_list_norm[[i]] = wt_slow_mtx_list[[1]][[i]]
}

for (i in 1:5) {
  for (j in 2:10) {
    cac_fast_mtx_list_norm[[i]] = cac_fast_mtx_list_norm[[i]] + cac_fast_mtx_list[[j]][[i]] * (cac_fast_chase40_sum[1] / cac_fast_chase40_sum[j])
    cac_slow_mtx_list_norm[[i]] = cac_slow_mtx_list_norm[[i]] + cac_slow_mtx_list[[j]][[i]] * (cac_slow_chase40_sum[1] / cac_slow_chase40_sum[j])
    
    wt_fast_mtx_list_norm[[i]] = wt_fast_mtx_list_norm[[i]] + wt_fast_mtx_list[[j]][[i]] * (wt_fast_chase40_sum[1] / wt_fast_chase40_sum[j])
    wt_slow_mtx_list_norm[[i]] = wt_slow_mtx_list_norm[[i]] + wt_slow_mtx_list[[j]][[i]] * (wt_slow_chase40_sum[1] / wt_slow_chase40_sum[j])
  }
  
}


scr.m = matrix(c(0.1, 0.5, 0.80, 0.95,
                 0.1, 0.5, 0.65, 0.80,
                 0.1, 0.5, 0.50, 0.65,
                 0.1, 0.5, 0.35, 0.50,
                 0.1, 0.5, 0.2, 0.35, #fast nuc stackup 
                 
                 0.5, 0.9, 0.80, 0.95,
                 0.5, 0.9, 0.65, 0.80,
                 0.5, 0.9, 0.50, 0.65,
                 0.5, 0.9, 0.35, 0.50,
                 0.5, 0.9, 0.20, 0.35, #slow nuc stackup
                 
                 0.51, 0.82, 0.08, 0.13 #legend
                 
                 
                 
),
ncol = 4, byrow = T
)

#plotting WT (2C)
#######
file.name <- "/data/home/bc202/cac/final/figure/figure2/figure2C_wt_stackup.png"
png(file = file.name, width = 11, height = 15, units = "in", res = 250, bg = "transparent", type = "cairo-png" )
par(cex=1.5)

close.screen(all.screens = T)

split.screen(scr.m)

par(oma = c(0, 3, 0, 0)) # make room (i.e. the 4's) for the overall x and y axis titles

for (f in 1:4) {
  screen(f)
  par(mar = c(0.5,0.5,0.5,0.5), cex=1, cex.main=0.7) 
  a = wt_fast_mtx_list_norm[[f]][30:230,100:500]
  dens_dot_plot(a[nrow(a):1,],  z_min = 0, z_max =900, plot_title='',x_axt = 'n', y_axt = 'n',lowCol = 'slateblue2', medCol = "yellow", highCol = "red", numColors = 50) #adjust z range for each experiment
  axis(side = 2,labels = F, tick = T, at = c(20,70,120,170))
  mtext(at=c(20,70,120,170), text=c(50,100,150,200), side=2, line=0.8, cex=1.5)
  
}


for (f in 6:9) {
  screen(f)
  par(mar = c(0.5,0.5,0.5,0.5), cex=1, cex.main=0.7) 
  a = wt_slow_mtx_list_norm[[f-5]][30:230,100:500] 
  dens_dot_plot(a[nrow(a):1,],  z_min = 0, z_max =900, plot_title='',x_axt = 'n', y_axt = 'n',lowCol = 'slateblue2', medCol = "yellow", highCol = "red", numColors = 50) #adjust z range for each experiment
}

screen(5)
par(mar = c(0.5,0.5,0.5,0.5), cex=1, cex.main=0.7) 
a = wt_fast_mtx_list_norm[[5]][30:230,100:500] 
dens_dot_plot(a[nrow(a):1,],  z_min = 0, z_max =900, plot_title='',x_axt = 'n', y_axt = 'n',lowCol = 'slateblue2', medCol = "yellow", highCol = "red", numColors = 50) #adjust z range for each experiment
axis(side = 2,labels = F, tick = T, at = c(20,70,120,170))
mtext(at=c(20,70,120,170), text=c(50,100,150,200), side=2, line=0.8, cex=1.5)
axis(side=1,labels=F, tick=T,at=c(-150,0,150)) #x-axis
mtext(at=c(-150,0,150), text=c(-150,0,150), side=1, line=0.8, cex=1.5)

screen(10)
par(mar = c(0.5,0.5,0.5,0.5), cex=0.8, cex.main=0.7) 
a = wt_slow_mtx_list_norm[[5]][30:230,100:500] 
dens_dot_plot(a[nrow(a):1,],  z_min = 0, z_max =900, plot_title='',x_axt = 'n', y_axt = 'n',lowCol = 'slateblue2', medCol = "yellow", highCol = "red", numColors = 50) #adjust z range for each experiment
axis(side=1,labels=F, tick=T,at=c(-150,0,150)) #x-axis
mtext(at=c(-150,0,150), text=c(-150,0,150), side=1, line=0.8, cex=1.5)


mtext('Fragment length (bp)', side = 2, outer = TRUE, cex=2.2, line=-1.1,at = 0.6)
mtext('Distance from nucleosome dyad (bp)', side = 1, outer = T, cex=2.2,line=-7.5,at = 0.5)


screen(11) #legend
eff_color2 <- matrix(c(0,100,200,300,400,500,600,700,800,900), nrow=1, ncol=10)
par(mar = c(0.8,0,0.8,0))
dens_dot_plot(eff_color2, z_min = 0, z_max = 900, plot_title="",lowCol='slateblue2',x_axt='n',y_axt = "n",medCol = "yellow", highCol = 'red',numColors = 50)
mtext(at=c(-3.5,-1.5,0.5,2.5,4.5), text=c(100,300,500,700,900), side=1,cex=1.5)

close.screen(all.screens = T)

dev.off()

#plotting cac (2C)
######
file.name <- "/data/home/bc202/cac/final/figure/figure2/figure2D_cac_stackup.png"
png(file = file.name, width = 11, height = 15, units = "in", res = 250, bg = "transparent", type = "cairo-png" )
par(cex=1.5)

close.screen(all.screens = T)

split.screen(scr.m)

par(oma = c(0, 3, 0, 0)) # make room (i.e. the 4's) for the overall x and y axis titles

for (f in 1:4) {
  screen(f)
  par(mar = c(0.5,0.5,0.5,0.5), cex=1, cex.main=0.7) 
  a = cac_fast_mtx_list_norm[[f]][30:230,100:500]
  dens_dot_plot(a[nrow(a):1,],  z_min = 0, z_max =900, plot_title='',x_axt = 'n', y_axt = 'n',lowCol = 'slateblue2', medCol = "yellow", highCol = "red", numColors = 50) #adjust z range for each experiment
  axis(side = 2,labels = F, tick = T, at = c(20,70,120,170))
  mtext(at=c(20,70,120,170), text=c(50,100,150,200), side=2, line=0.8, cex=1.5)
  
}


for (f in 6:9) {
  screen(f)
  par(mar = c(0.5,0.5,0.5,0.5), cex=1, cex.main=0.7) 
  a = cac_slow_mtx_list_norm[[f-5]][30:230,100:500]
  dens_dot_plot(a[nrow(a):1,],  z_min = 0, z_max =900, plot_title='',x_axt = 'n', y_axt = 'n',lowCol = 'slateblue2', medCol = "yellow", highCol = "red", numColors = 50) #adjust z range for each experiment
}

screen(5)
par(mar = c(0.5,0.5,0.5,0.5), cex=1, cex.main=0.7) 
a = cac_fast_mtx_list_norm[[5]][30:230,100:500]
dens_dot_plot(a[nrow(a):1,],  z_min = 0, z_max =900, plot_title='',x_axt = 'n', y_axt = 'n',lowCol = 'slateblue2', medCol = "yellow", highCol = "red", numColors = 50) #adjust z range for each experiment
axis(side = 2,labels = F, tick = T, at = c(20,70,120,170))
mtext(at=c(20,70,120,170), text=c(50,100,150,200), side=2, line=0.8, cex=1.5)
axis(side=1,labels=F, tick=T,at=c(-150,0,150)) #x-axis
mtext(at=c(-150,0,150), text=c(-150,0,150), side=1, line=0.8, cex=1.5)

screen(10)
par(mar = c(0.5,0.5,0.5,0.5), cex=0.8, cex.main=0.7) 
a = cac_slow_mtx_list_norm[[5]][30:230,100:500]
dens_dot_plot(a[nrow(a):1,],  z_min = 0, z_max =900, plot_title='',x_axt = 'n', y_axt = 'n',lowCol = 'slateblue2', medCol = "yellow", highCol = "red", numColors = 50) #adjust z range for each experiment
axis(side=1,labels=F, tick=T,at=c(-150,0,150)) #x-axis
mtext(at=c(-150,0,150), text=c(-150,0,150), side=1, line=0.8, cex=1.5)


mtext('Fragment length (bp)', side = 2, outer = TRUE, cex=2.2, line=-1.1,at = 0.6)
mtext('Distance from nucleosome dyad (bp)', side = 1, outer = T, cex=2.2,line=-7.5,at = 0.5)


screen(11)
eff_color2 <- matrix(c(0,100,200,300,400,500,600,700,800,900), nrow=1, ncol=10)
par(mar = c(0.8,0,0.8,0))
dens_dot_plot(eff_color2, z_min = 0, z_max = 900, plot_title="",lowCol='slateblue2',x_axt='n',y_axt = "n",medCol = "yellow", highCol = 'red',numColors = 50)
mtext(at=c(-3.5,-1.5,0.5,2.5,4.5), text=c(100,300,500,700,900), side=1,cex=1.5)

close.screen(all.screens = T)

dev.off()
######


###
#supplemental 2 (chrIV is representative of the genome)
file.name <- "/data/home/bc202/cac/final/figure/supple2_chrIV_nuc.png"
png(file = file.name, width =5, height = 4, units = "in", res = 200, bg = "white", type = "cairo-png" )
par(mar = c(4,4,4,2), cex=0.8)
plot(density(nuc_master_sub$wt_ATI),main='',xlab='ATI',lwd=3,col=wt_col,xlim=c(1.2,1.9))
lines(density(nuc_master_sub$wt_ATI[which(nuc_master_sub$chr=='chrIV')]),lty=2,lwd=3,col=wt_col)
lines(density(nuc_master_sub$cac_ATI),lwd=3,col=cac_col)
lines(density(nuc_master_sub$cac_ATI[which(nuc_master_sub$chr=='chrIV')]),lty=2,lwd=3,col=cac_col)
legend('topleft',c('WT, all nucleosomes',expression(paste(italic(cac1),Delta,', all nucleosomes')),'WT, ChrIV nucleosomes',expression(paste(italic(cac1),Delta,', ChrIV nucleosomes'))),lty=c(1,1,2,2),col=c(wt_col,cac_col,wt_col,cac_col),lwd=c(2,2,2,2),
       box.lwd = 0,box.col = "transparent",bg = "transparent")

dev.off()

####
#supplemental 3 (summed occupancy of fast and slow nucleosomes )
cac_fast_v = colSums(cac_fast_nuc[,8:12])
cac_slow_v = colSums(cac_slow_nuc[,8:12])
wt_fast_v = colSums(wt_fast_nuc[,3:7])
wt_slow_v = colSums(wt_slow_nuc[,3:7])
png(file = '~/cac/final/figure/supple3_slowFast_sum.png', width =5, height = 4, units = "in", res = 200, bg = "white", type = "cairo-png" )
par(mar = c(4,4,4,2), cex=1)
plot(c(1,2,3,4,5),cac_slow_v,ylim=c(200000,1500000),ylab='Summed occupancy',xaxt = 'n',col='tomato3',pch=20,cex=2,xlab='')
points(c(1,2,3,4,5),cac_fast_v,col='forestgreen',pch=20,cex=2)
points(c(1,2,3,4,5),wt_slow_v,ylim=c(200000,1500000),ylab='Summed occupancy',xaxt = 'n',col='tomato3',pch=10,cex=1.5,xlab='')
points(c(1,2,3,4,5),wt_fast_v,col='forestgreen',pch=10,cex=1.5)
axis(side = 1, at = c(1,2,3,4,5),labels = c('Pulse',"10'","15'","20'","40'"))
legend('bottomright',c('Slow','Fast','WT',expression(paste(italic(cac1),Delta))),col=c('tomato3','forestgreen','black','black'),pch=c(20,20,10,20),bty="n")
dev.off()
######








