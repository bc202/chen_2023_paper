# Chen_2023_paper
Contact: 
Boning Chen, bc202@duke.edu

General notes:
This repository contains scripts and other files needed to run figures from 2023 paper. BAM files must be downloaded from SRA (https://www.ncbi.nlm.nih.gov/sra) with accession number PRJNA974469  to run all scripts. If applicalbe, the figure directories would contain the 'master' code needed to process the data before plotting. Some preprocessed data can also be found in the 'csv' directory. Other datasets necessary to run scripts are contained in 'csv' and 'Functions'.

